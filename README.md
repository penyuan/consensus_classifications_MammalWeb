# Economical camera trap image classifications

> Consensus classifications for crowdsourced camera trap data.

This [RStudio](https://www.rstudio.com/) project and included R script are used to derive and analyse consensus classifications from crowdsourced classifications of camera trap data. This repository is associated with the paper "[Economical crowdsourcing for camera trap image classification](https://doi.org/10.1002/rse2.84)" in the journal [Remote Sensing in Ecology and Conservation](https://zslpublications.onlinelibrary.wiley.com/journal/20563485).

## Install

Before use:

1. Create the `figures` and `tables` folders in the same folder as `consensus_classifications.R` for figure and table outputs. 
2. Create a `data` folder alongside `output` where input data file will be placed. The script is designed to work with the data file `classifications.csv` from [MammalWeb](http://www.MammalWeb.org/) which can be found in [this repository](https://osf.io/znm6k/). This data file contains user-supplied classifications of image sequences uploaded to MammalWeb.

The R package [`packrat`](https://rstudio.github.io/packrat/rstudio.html) is used by the included R project file (`consensus_classifications_MammalWeb.Rproj`). This means when first running the project in Rstudio, `packrat` will populate the project with the required R package dependencies.

## Usage

Run `consensus_classifications.R` after loading the project in [RStudio](https://www.rstudio.com/). Look in the `figures` and `tables` folders for figure and table outputs.

## License

All code in this repository is shared under the GNU GPLv3 license or later versions. See the file LICENSE.